const express = require('express')
const bodyParser = require('body-parser');
const mysql = require('mysql');
const cors = require('cors')
const app = express();

const port = 8000;

app.listen(port, () => {
    console.log("We are live on " + port) ; 
})

var connection = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'password',
    database : 'sys'
});

connection.connect(function(error) {
    if(error){
        console.log('Error');
    } else {
        console.log('Connected');
    }
})

app.get('/', function(req, res) {
    connection.query("SELECT * FROM foodApp.restaurant;", function(error, rows, fields) {
        if(error){
            console.log('Error in Query')
        } else {
            console.log('Success in Query')
            console.log(rows)
        }
    });
})

app.get('/getRestaurant/:loc', cors(), function(req, res) {
    var sql = 'SELECT * FROM foodApp.restaurant where location_id = ?'
    var loc_id = req.params.loc
    connection.query(sql, [loc_id], function(error, rows, fields) {
        if(error){
            console.log('Error in Query')
        } else {
            console.log('Success in Query')
            console.log(rows)
            console.log('Returning rows')
            res.send(rows)      
            //res.status(200).json(response)      
        }
    });
})

app.get('/getCategory', cors(), function(req, res) {
   // var sql = 'SELECT * FROM foodApp.category'
   
    connection.query("SELECT * FROM foodApp.category;", function(error, rows, fields) {
        if(error){
            console.log('Error in Query')
        } else {
            console.log('Success in Query')
            console.log(rows)
            console.log('Returning rows')
            res.send(rows)      
            //res.status(200).json(response)      
        }
    });
})

app.get('/getMenu', cors(), function(req, res) {
    var sql = 'SELECT * FROM foodApp.menu_item'; 
   
    connection.query(sql, function(error, rows, fields) {
        if(error){
            console.log('Error in Query')
        } else {
            console.log('Success in Query')
            console.log(rows)
            console.log('Returning rows')
            res.send(rows)      
            //res.status(200).json(response)      
        }
    });
})

app.get('/updateOrder/:name/:number/:address/:price/:itemsOrdered', cors(), function(req, res) {

    var sql = "INSERT INTO foodApp.order_details( name, number, address, price, itemsOrdered) VALUES (?,?,?,?,?)";
    var loc_id = [req.params.name, req.params.number, req.params.address, req.params.price, req.params.itemsOrdered]
    connection.query(sql, loc_id, function(error, rows, fields) {
        if(error){
            console.log('Error in Query'+ error)
        } else {
            console.log("1 record inserted, ID: " + rows.insertId);
            console.log('Success in Query')
            console.log(rows)
            console.log('Returning rows')
            res.send(rows)      
            //res.status(200).json(response)      
        }
    });
})
